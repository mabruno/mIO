function res = mIOread(fname)
%MIOREAD Summary of this function goes here
%   Detailed explanation goes here

fid = fopen(fname,'r');
if fid<0
    error('import_dat:fileNotFound',['Unable to open file ' fname '.'])
end
% test endianess end reopen with the right machineformat
byte = fread(fid,1,'*char');
fclose(fid);
[fid, mf] = mfopen(fname,byte);

res=struct();

len=fread(fid,1,'int32',0,mf);   
while ~feof(fid)
    [field,data] = read_next_field(fid,mf,len);
    if ~isfield(res,field)
        res.(field) = {data};
    else
        res.(field){end+1} = data;
    end
    len=fread(fid,1,'int32',0,mf);   
end

fn = fieldnames(res);
for k=1:numel(fn)
    res.(fn{k}) = cell2mat(res.(fn{k}))';
end

end


function [field,data] = read_next_field(f,mf,len)

tag=fread(f,len+1,'*char',0,mf);
field=tag(1:end-1)'; type=tag(end);
n=fread(f,1,'int32',0,mf);

if type=='c'
    data=fread(f,n,'*char',0,mf);
elseif type=='i'
    data=fread(f,n,'int32',0,mf);
elseif type=='d'
    data=fread(f,n,'double',0,mf);
else
    error('type not recognized, corrupted dat file');
end

end


function [fid, machineformat]= mfopen(fname,byte)
if byte == 0
  fid = fopen(fname,'r','ieee-be');
  machineformat='ieee-be';
else
  fid = fopen(fname,'r','ieee-le');
  machineformat='ieee-le';
end
end