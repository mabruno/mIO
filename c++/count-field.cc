#include <stdlib.h>

#include "mIO.h"

int main(int argc,char *argv[])
{
   int cc;
   printf("[Usage]: ./count-field file.dat field\n");
   if (argc!=3)
      exit(1);

   mIO f(argv[1]);
   cc=f.count(argv[2]);
   printf("Field %s found %d times\n",argv[2],cc);

   return 0;
}
