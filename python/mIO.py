import struct
import os.path
import mmap

class mIO:
    def __init__(self,name,endian='<'):
        self.fname = name
        self.endian = endian
        self.isfile = os.path.isfile(name)

    def __reader(self,type_r,n,f):
        if type_r is 'c':
            var = str(f.read(n))
        elif type_r is 'i':
            var = struct.unpack(self.endian+'i'*n,f.read(4*n))
        elif type_r is 'd':
            var = struct.unpack(self.endian+'d'*n,f.read(8*n))
        return var
    
    def __packer(self,type_r,n,f):
        return struct.pack(self.endian+type_r*n,*f)
    
    def __get_type(self,f):
        if isinstance(f,str):
            return 'c'
        elif isinstance(f,int):
            return 'i'
        elif isinstance(f,float):
            return 'd'

    def __seeker(self,type_r,n,f):
        if type_r is 'c':
            f.seek(n,1)
        elif type_r is 'i':
            f.seek(4*n,1)
        elif type_r is 'd':
            f.seek(8*n,1)


    def exists(self):
        return self.isfile


    def write(self,tag,field):
        err_msg='field must be a string or a list of ints or floats'
        if isinstance(field,str):
            type_w='c'
        elif isinstance(field,(int,float)):
            type_w=self.__get_type(field)
            field=[field]
        else:
            if isinstance(field,list):
                if isinstance(field[0],str):
                    raise ValueError(err_msg)
                elif isinstance(field[0],(int,float)):
                    type_w=self.__get_type(field[0])
                else:
                    raise ValueError(err_msg)
            else:
                raise ValueError(err_msg)

        with open(self.fname,'ab') as f:
            f.write(self.__packer('i',1,[len(tag)]))
            f.write(self.__packer('c',len(tag),tag))
            f.write(struct.pack(self.endian+'c',type_w))
            f.write(self.__packer('i',1,[len(field)]))
            f.write(self.__packer(type_w,len(field),field))
        f.close()
        
        
    def read(self,tag,idx=None):
        res = []
        count = 0
        if not self.isfile:
            return []
        with open(self.fname,'rb') as f:
            while True:
                bb = f.read(4)
                if not bb:
                    break
                else:
                    [n] = struct.unpack(self.endian+'i',bb)
                tag_r = struct.unpack(self.endian+'c'*n,f.read(n))
                (type_r,) = struct.unpack(self.endian+'c',f.read(1))
                [n] = struct.unpack(self.endian+'i',f.read(4))
                
                if tag==''.join(tag_r):
                    if idx==None:
                        var = self.__reader(type_r,n,f)
                        res.append( var )
                    else:
                        if count==idx:
                            res = self.__reader(type_r,n,f)
                            break
                        else:
                            self.__seeker(type_r,n,f)
                    count += 1
                else:
                    self.__seeker(type_r,n,f)
        return res
    
    def get(self,tag,idx=None):
        res = []
        count = 0
        if not self.isfile:
            return []
        with open(self.fname,'rb') as f:
            while True:
                bb = f.read(4)
                if not bb:
                    break
                else:
                    [n] = struct.unpack(self.endian+'i',bb)
                tag_r = struct.unpack(self.endian+'c'*n,f.read(n))
                (type_r,) = struct.unpack(self.endian+'c',f.read(1))
                [n] = struct.unpack(self.endian+'i',f.read(4))
                
                if tag==''.join(tag_r):
                    if idx==None:
                        var = self.__reader(type_r,n,f)
                        if (len(var)==1):
                            res.append( var[0] )
                        else:
                            res.append( list(var) )
                    else:
                        if count==idx:
                            var = self.__reader(type_r,n,f)
                            if len(var)==1:
                                res.append( var[0] )
                            else:
                                res.append( list(var) )
                            break
                        else:
                            self.__seeker(type_r,n,f)
                    count += 1
                else:
                    self.__seeker(type_r,n,f)
        if (len(res)==1):
            return res[0]        
        return res


    def load(self,memmap=True):
        res = {}
        ff = self.fields()
        for fff in ff:
            res[fff] = []

        if not self.isfile:
            return {}
        with open(self.fname,'rb') as f:
            if memmap:
                mm=mmap.mmap(f.fileno(),0,prot=mmap.PROT_READ)
            else:
                mm=f
            while True:
                bb = mm.read(4)
                if not bb:
                    break
                else:
                    [n] = struct.unpack(self.endian+'i',bb)
                tag_r = struct.unpack(self.endian+'c'*n,mm.read(n))
                (type_r,) = struct.unpack(self.endian+'c',mm.read(1))
                [n] = struct.unpack(self.endian+'i',mm.read(4))
                
                var = self.__reader(type_r,n,mm)
                res[''.join(tag_r)].append( var )
        return res
    
    def __str__(self):
        res = ''
        if not self.isfile:
            return 'file not found'
        with open(self.fname,'rb') as f:
            while True:
                bb = f.read(4)
                if not bb:
                    break
                else:
                    [n] = struct.unpack(self.endian+'i',bb)
                tag_r = struct.unpack(self.endian+'c'*n,f.read(n))
                (type_r,) = struct.unpack(self.endian+'c',f.read(1))
                [n] = struct.unpack(self.endian+'i',f.read(4))
                res += '\n' + ''.join(tag_r) + ' = '
                if type_r is 'c':
                    res += ' ' + str(f.read(n))
                else:
                    if type_r is 'i':
                        var = struct.unpack(self.endian+'i'*n,f.read(4*n))
                    elif type_r is 'd':
                        var = struct.unpack(self.endian+'d'*n,f.read(8*n))
                    for v in var:
                        res += ' ' + str(v) 
        return res

    def fields(self):
        res = []
        if not self.isfile:
            return []
        with open(self.fname,'rb') as f:
            while True:
                bb = f.read(4)
                if not bb:
                    break
                else:
                    [n] = struct.unpack(self.endian+'i',bb)
                tag_r = struct.unpack(self.endian+'c'*n,f.read(n))
                (type_r,) = struct.unpack(self.endian+'c',f.read(1))
                [n] = struct.unpack(self.endian+'i',f.read(4))
                field = ''.join(tag_r)
                if field not in res:
                    res.append(field)
                self.__seeker(type_r,n,f)
        return res
    
    def howmany(self,tag):
        count = 0
        if not self.isfile:
            return -1
        with open(self.fname,'rb') as f:
            while True:
                bb = f.read(4)
                if not bb:
                    break
                else:
                    [n] = struct.unpack(self.endian+'i',bb)
                tag_r = struct.unpack(self.endian+'c'*n,f.read(n))
                (type_r,) = struct.unpack(self.endian+'c',f.read(1))
                [n] = struct.unpack(self.endian+'i',f.read(4))
                self.__seeker(type_r,n,f)

                if tag==''.join(tag_r):
                    count+=1
        return count
