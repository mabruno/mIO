from mIO import mIO

m = mIO("../test.dat")

n=10
b=45

vec=[3.14*i for i in range(n)]
f1=[-1.2345,+3.4567]
f2=[-5.6789,+10.1112]

text="here-goes-some-text"

m.write("variable1",b)
m.write("vector-length",n)
m.write("mytext",text)
m.write("myvector",vec);
m.write("myfield",f1);
m.write("myfield",f2);

######

m2 = mIO("../test.dat")

n = m2.read("vector-length")
vec = m2.read("myvector")
print 'reading ', n[0], vec[0]

text = m2.read("mytext");
print 'reading ', text

print m2.read("not-this-field")

print 'and printing '

print m2

print m2.fields()

print m2.read("myfield",0)
print m2.read("myfield",1)
print m2.read("myfield",2)
